<?php class AuthUser extends CI_Model{
	public function autenticarUsuario($token){
		//verifica se o token que está vindo pelo header é válido			
		$user = $this->db->select('*')->where('token', $token)->get('admin')->row();
		if(!$user){
			$this->output->set_content_type('application/json')->set_status_header(401);		
			exit();
		}
		unset($user->password);
		return $user;
	}		
}?>