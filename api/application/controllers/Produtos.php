<?php class Produtos extends CI_Controller
{
    public function verificarToken() {
        $this->AuthUser->autenticarUsuario($this->input->get_request_header('token'));
    }

    public function atualizar() {
        $this->verificarToken();
        $update = [
			'id' => $this->input->post('DT_RowId'),
            'nome' => $this->input->post('nome'),
            'fabricante' => $this->input->post('fabricante'),
            'preco' => $this->input->post('preco'),
            'descricao' => $this->input->post('descricao'),
            'quantidade' => $this->input->post('quantidade')
        ];
            
        $res = $this->db->where('id', $update['id'])
                ->update('produtos', $update);
        if (!$res) {
            return $this->output->set_content_type('application/json')->set_status_header(400);
            exit();
        }
        
            return $this->output->set_content_type('application/json')->set_output(json_encode($update['id']));
    }

    //Para para popular o Datatables
    public function get() {
		$user = $this->verificarToken();
        $res = $this->db->select('id as DT_RowId, nome, fabricante, preco, descricao, quantidade')->get('produtos')->result();
        if(!$res){
            return $this->output->set_content_type('application/json')->set_status_header(400);
            exit();
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

    public function deletar() {
        $this->verificarToken();
        $res = $this->db->where('id', $this->input->post('DT_RowId'))->delete('produtos');
        if (!$res) {
            return $this->output->set_content_type('application/json')->set_status_header(400);
            exit();
        }

        return $this->output->set_content_type('application/json')->set_output($res);
    }

    public function adicionar() {
        //dados para o insert
        $this->verificarToken();
        $produto = array(
            'nome' => $this->input->post('nome'),
            'fabricante' => $this->input->post('fabricante'),
            'preco' => $this->input->post('preco'),
            'descricao' => $this->input->post('descricao'),
            'quantidade' => $this->input->post('quantidade'),
        );

		$res = $this->db->insert('produtos', $produto);
		
		if(!$res) {
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}

        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
}
