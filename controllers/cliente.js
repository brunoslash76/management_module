app.controller('cliente', function ($scope, $rootScope, $http, $httpParamSerializerJQLike) {
    $rootScope.load();
    var ID_TABELA = '#tabela';

    // FUNCAO PARA CARREGAR TABELA
    $('#example').DataTable({
        "order": [[1, "desc"]]
    });

    var tabela = $(ID_TABELA).DataTable({
        'ajax': {
            'url': 'api/clientes/get',
            'type': 'POST',
            'beforeSend': function (request) {
                request.setRequestHeader("token", localStorage.token);
            },
            dataSrc: '',
            'DT_RowId': 'id'
        },
        columns: [
            {
                render: function (a, b, c, d, e) {
                    var ligado = "off";
                    var colName = "";
                    if (c.ativo === "1") {
                        colName = '<span id="span-' + c.DT_RowId + '" class="sort-hidden">a</span> <div value="1" id="a' + c.DT_RowId + '" class="greenLight on"></div>';
                    } else {
                        colName = '<span id="span-' + c.DT_RowId + '" class="sort-hidden">b</span> <div value="0" id="a' + c.DT_RowId + '" class="greenLight off"></div>';
                    }
                    return colName;
                }
            },
            { data: "name" },
            { data: "email" },
            { data: "phoneNumber" },
            {
                render: function (data, a, b, c) {
                    var idcb = b.DT_RowId;
                    var checkbox = "";
                    if (b.ativo === "1") {
                        checkbox = '<span class="sort-hidden">1</span><input type="checkbox" id="m' + idcb + '" class="mtz-switcher" checked>';
                    } else if (b.ativo === "0") {
                        checkbox = '<span class="sort-hidden">0</span><input type="checkbox" id="m' + idcb + '" class="mtz-switcher">';
                    }
                    return '<label class="switch">' + checkbox + '<span class="slider"></span></label> ';
                }
            },
            {
                render: function (data) {
                    return "<div class='row'><div class='col-md-4'><button class='btn btn-primary btnEdit' id='butn-edit' data-toggle='modal' data-target='#editarCliente' ><i class='fa fa-pencil-square-o fa-2'></i></button></div><div class='col-md-4'><button class='btn btn-danger btnDelete' ><i class='fa fa-trash fa-2'></i></button></div></div>";
                }
            }
        ],
        "columnDefs": [
            { "targets": [3, 4, 5], "searchable": false, "orderable": false, "visible": true }
        ]
    });
    // METODOS DO CONTROLLER
    $scope.addCliente = function () {
        if ($scope.formc.ativo === true) {
            $scope.formc.ativo = "1";
        } else {
            $scope.formc.ativo = "0";
        }
        $http({
            method: 'POST',
            url: 'api/clientes/adicionar',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'token': localStorage.token
            },
            data: $httpParamSerializerJQLike($scope.formc),
        }).then(function (result) {
            $scope.formc = {};
            $(ID_TABELA).DataTable().ajax.reload();
            swal({
                title: 'Cadastro efetuado com sucesso',
                text: 'Se não quiser adicionar outro cliente clique em cancelar!',
                type: 'success',
                showConfirmButton: false,
                timer: 1500
            });
        }, function (error) {
            swal('Oops', 'Erro detectado, por favor refaça a operação');
            console.error(error);
        });
    };

    //Abrir modal para edição
    $(ID_TABELA + ' tbody').on('click', '.btnEdit', function () {
        $scope.form = tabela.row($(this).parents('tr')).data();
        $scope.$apply();
    });

    // ATUALIZAR CLIENTE
    $scope.update = function () {
        $http({
            url: 'api/clientes/atualizar',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'token': localStorage.token,
            },
            data: $httpParamSerializerJQLike($scope.form)
        }).then(function (result) {
            $scope.form = {};
            $(ID_TABELA).DataTable().ajax.reload();
            $('#editarCliente').modal('toggle');
            swal({
                position: 'center',
                type: 'success',
                title: 'Cliente atualizado!',
                showConfirmButton: false,
                timer: 1500
            });
        });
    };

    //DELETAR CLIENTE
    $(ID_TABELA + ' tbody').on('click', '.btnDelete', function () {
        $scope.user = tabela.row($(this).parents('tr')).data();
        $scope.$apply();
        swal({
            title: 'Tem certeza que vai deletar?',
            text: "Essa ação não pode ser desfeita!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Deletar',
        }).then(function () {
            $http({
                url: 'api/clientes/deletar',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'token': localStorage.token,
                },
                data: $httpParamSerializerJQLike($scope.user)
            }).then(function (result) {
                $(ID_TABELA).DataTable().ajax.reload();
                swal({
                    position: 'center',
                    type: 'success',
                    title: 'Cliente deletado!',
                    showConfirmButton: false,
                    timer: 1500
                });
            });
        });
    });
    // AUTORIZAR / DESAUTORIZAR
    $(ID_TABELA + ' tbody').on('click', '.mtz-switcher', function (event) {
        $scope.autorizar = tabela.row($(this).parents('tr')).data();
        var input = $('#m' + $scope.autorizar.DT_RowId);
        var greenLight = $('#a' + $scope.autorizar.DT_RowId);
        var spanSort = $('#span-' + $scope.autorizar.DT_RowId);
        var nomeUsuario = ($scope.autorizar.name).toUpperCase();
        var checked = input.is(":checked");
        var token = $scope.autorizar.token;

        if (checked) {
            console.log('usuario ativo');
            $(greenLight).addClass('on');
            $(greenLight).removeClass('off');
            $(spanSort).empty();
            $(spanSort).append('a');
            var data = $.param({
                'token' : $scope.autorizar.token   
            });
            $http({
                url: 'api/clientes/autorizar',
                method: 'post',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': localStorage.token
                },
                data: data
            })
            .then(function () {
                $(ID_TABELA).DataTable().ajax.reload();
                swal({
                    title: nomeUsuario + ' autorizado pelo administrador!',
                    showCancelButton: false,
                    showConfirmButton: false,
                    type: 'success',
                    timer: 1500
                });
            });
            enviarEmail(token);
        } else if (!checked) {
            $(greenLight).addClass('off');
            $(greenLight).removeClass('on');
            $(spanSort).empty();
            $(spanSort).append('b');
            var data = $.param({
                'token' : $scope.autorizar.token   
            });
            $http({
                url: 'api/clientes/desautorizar',
                method: 'post',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': localStorage.token
                },
                data: data
            })
            .then(function () {
                $(ID_TABELA).DataTable().ajax.reload();
                swal({
                    title: nomeUsuario + ' não está mais autorizado!',
                    showCancelButton: false,
                    showConfirmButton: false,
                    type: 'info',
                    timer: 1500
                });
            });
            enviarEmail(token);
        }
    });

    function enviarEmail(token) {
        var t = $.param({
            'token': token
        });
        $http({
            url: 'api/clientes/enviarEmail',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'token': localStorage.token
            },
            data: t
        })
        .then(function (result) { }, function (error) {
            console.error(error);
        });
    }
    // MASKS FOR INPUTS
    $('.cpf').mask('000.000.000-00');
    $('.telefone').mask('(00) 00000-0000');
});// 