app.controller('produtos', function ($scope, $http, $httpParamSerializerJQLike) {
    var ID_TABELA = '#tabelaProdutos';
    // CARREGA DADOS DA TABELA
    var tabela = $(ID_TABELA).DataTable({
        'ajax': {
            'url': 'api/produtos/get',
            'type': 'POST',
            'beforeSend': function (request) {
                request.setRequestHeader("token", localStorage.token);
            },
            dataSrc: '',
            'DT_RowId': 'id'
        },
        columns: [
            { data: 'nome' },
            { data: 'fabricante' },
            { data: 'preco' },
            { data: 'descricao' },
            { data: 'quantidade' },
            {
                render: function () {
                    return "<div class='row'><div class='col-md-4'><button class='btn btn-primary btnEditP' data-toggle='modal' data-target='#editarProduto'><i class='fa fa-pencil-square-o fa-2'></i></button></div><div class='col-md-4'><button class='btn btn-danger btnDeleteP' ><i class='fa fa-trash fa-2'></i></button></div></div>";
                }
            }
        ]
    });

    // METODOS DO CONTROLLER
    $scope.addProduto = function () {
        //VALIDATOR BUGGADO -> DOCS
        // $('#formProduto').formValidation({
        //     icon: {
        //         valid: 'mdi mdi-check',
        //         invalid: 'mdi mdi-close',
        //         validating: 'fa fa-refresh'
        //     }
        // }.on('success.from.fv', function (e) {
        //     e.preventDefault();
        // }));

        $http({
            url: 'api/produtos/adicionar',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'token': localStorage.token
            },
            data: $httpParamSerializerJQLike($scope.formp)
        }).then(function (result) {
            $scope.formp = {};
            $(ID_TABELA).DataTable().ajax.reload();
            swal({
                title: 'Cadastro efetuado com sucesso',
                text: 'Se não quiser adicionar outro produto clique em cancelar!',
                type: 'success',
                showConfirmButton: false,
                timer: 1500
            }, function (error) { 
                console.log('deu ruim');
            });
        }, function (error) { 
            if(error.status === 400) { 
                swal('Oops', 'Deu erro!', 'error');
            }
        });
    };
    // EVENTO DO BOTAO DE EDITAR LINHA
    $(ID_TABELA + ' tbody').on('click', '.btnEditP', function () {
        $scope.formpr = tabela.row($(this).parents('tr')).data();
        $scope.formpr.preco = parseFloat($scope.formpr.preco); 
        $scope.formpr.quantidade = parseInt($scope.formpr.quantidade);
        console.log($scope.formpr);
        $scope.$apply();
    });

    $scope.updateProduto = function () {
        $http({
            url: 'api/produtos/atualizar',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'token': localStorage.token,        
            },
            data: $httpParamSerializerJQLike($scope.formpr)
        }).then(function () {
            $scope.formpr = {};
            $(ID_TABELA).DataTable().ajax.reload();
            $('#editarProduto').modal('toggle');
            swal({
                position: 'center', type: 'success', title: 'Produto atualizado!', showConfirmButton: false, timer: 1500
            });
        });
    };

    // EVENTO DO BOTAO DELETE DA LINHA 
    $(ID_TABELA + ' tbody').on('click', '.btnDeleteP', function () {
        console.warn('entrou no evento de click...');
        $scope.produto = tabela.row($(this).parents('tr')).data();
        $scope.$apply();
        swal({
            title: 'Tem certeza que deseja deletar este produto?',
            text: 'Esta  ação não pode ser desfeita',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Deletar',
        }).then(function () {
            $http({
                url: 'api/produtos/deletar',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'token': localStorage.token
                },
                data: $httpParamSerializerJQLike($scope.produto)
            }).then(function (result) {
                $(ID_TABELA).DataTable().ajax.reload();
                swal({
                    position: 'center', type: 'success', title: 'Produto deletado!', showConfirmButton: false, timer: 1500
                });
            });
        });
    });
});//