app.controller('recuperar', function ($scope, $http, $state, $httpParamSerializerJQLike) {
    $scope.verificaEmail = function () {        
        $http({
            method: 'POST',
            url: 'api/auth/verificarEmail',
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: $httpParamSerializerJQLike($scope.recuperar)
            
        })
            // success
            .then(function (result) {
                $scope.passRecover = result.data;
                $scope.testeEmail = true;
            },
            // error
            function (error) {
                swal('Oops', 'Email não cadastrado!', 'error');
            });
    };

    $scope.verificaToken = function () {        
        $http({
            method: 'POST',
            url: 'api/auth/verificarToken',
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data:  $httpParamSerializerJQLike($scope.recuperar)
            
        })
            .then(function (result) {
                $scope.testeToken = true;
            },
            function (error) {
                swal({
                    type: 'error',
                    title: 'Token inválido!',
                    timer: 1000
                });
            });
    };

    $scope.registrarNovaSenha = function() {
        $http({
            method: 'POST',
            url: 'api/auth/alterarSenha',
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: $httpParamSerializerJQLike($scope.recuperar)
            
        })
        //success
        .then(function(result){
            localStorage.token = result.data;
            $state.go('app.home');
        });
    };
});//