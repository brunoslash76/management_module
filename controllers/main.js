app.controller('main', function ($http, $rootScope, $state, $interval) {
    $rootScope.logout = function () {
        localStorage.clear();
        $state.go('acessibilidade.login');
    };

    $rootScope.load = function () {
        $rootScope.localToken = localStorage.token;
        $rootScope.userInfo = {};
        $http({
            method: 'POST',
            url: 'api/profile',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'token': localStorage.token
            }
        })
        .then(function(response){
            $rootScope.userInfo = localStorage.dataUser = response.data;
            $rootScope.userInfo.birthdate = response.data.birthdate;
        },
        function(error){
            swal('Ops', 'Você foi desconectado!', 'error');
            $state.go('acessibilidade.login');
        });
    };

    var hora = function () {
        $rootScope.hora = Date.now();
    };
    hora();
   $interval(hora, 1000); 
});