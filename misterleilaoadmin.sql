-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 27/11/2017 às 12:47
-- Versão do servidor: 5.7.20
-- Versão do PHP: 7.0.20-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `misterleilaoadmin`
--
CREATE DATABASE IF NOT EXISTS `misterleilaoadmin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `misterleilaoadmin`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(60) NOT NULL,
  `token` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `token`) VALUES
(1, 'Elton Bezerra', 'elton.cin@hotmail.com', '$2y$10$mVKUfkpzuyRiZ09lT/i43utkdxZ/RM/i9..rJf1JknMaR3VIZ1HnW', '$2y$10$Mn8TEcXSO6UUS7rXVjHOneHk9uFVkH4QT/TJO17KgWixFaA9d1XL.'),
(2, 'Elton Bezerra da Silva', 'elton.dyf@gmail.com', '$2y$10$l1mY5.bS.1nvuHeFqPzzCuHWqPAEdP5mChjbiBxX5GfZzbAxSrVou', '$2y$10$qM6cDPL9aplIFTwYP94tO.Hd51663ZO/mp6N/3bIo8EM4Daxk4UIe');

-- --------------------------------------------------------

--
-- Estrutura para tabela `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phoneNumber` varchar(11) DEFAULT NULL,
  `password` varchar(60) NOT NULL,
  `token` varchar(60) NOT NULL,
  `cpf` varchar(11) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `clientes`
--

INSERT INTO `clientes` (`id`, `name`, `email`, `phoneNumber`, `password`, `token`, `cpf`, `ativo`) VALUES
(1, 'Elton Bezerra', 'elton.cin@hotmail.com', '959362416', '$2y$10$ANuxndAQMkM7pMmyJ6AV9ey71CpIYPWmrClH2DmDghctTvTnVhfAW', '$2y$10$K88EmK.STHYBoAdbMdFQEuqt0TQhlHu6CjkyGkIjyvtYgMfS04EeO', '12345678911', 1),
(2, 'Bruno Viado', 'andreas@gmail.com.br', '12312312', '$2y$10$Y1Sh1tAbf8HQYbOth8uvueby9A4RKQc1wugDqxHm2cO0EfbWb/wYW', '$2y$10$QGUNuLt2MD46DUpy48h/B..EAMY9xDxUvm72OLhIDQmZovkwUpk9K', '228.046.038', 0),
(3, 'Bruno Tavares de Moraes Russo', 'bdmrusso@gmail.com', '123123123', '$2y$10$rRqHytzF9Ps7wuW.TrTG9.eA1kVur5gEQSsrwlBD9xWI2Xss4Ll1y', '$2y$10$DJfqUkAvfDHVrKp2dXxgMOulIXkxCHi1fItuPIEBvpTVRnFxIZ1DC', '344.876.772', 0),
(4, 'Jõao Jeferson', 'ammina@gmail.com', '123123123', '$2y$10$DS/i/R7qClIVsNm2LP5ziuKDPJxmGO1iM0k.brwPX6mgUF.kFFpUG', '$2y$10$eDLRiNim4OxSY3x198Q.OeCh471t0KpWwKfvztYfXwjJcLety.jX.', '228.046.038', 0),
(5, 'Anibal Junior Matahata', 'asdas@huasuhsahu.coi', '12312312333', '$2y$10$rlT1vAe2wsyhshB/WUo2IuYZBlTnBonW1jZUhY0dSnt5PFJ2ASnhm', '$2y$10$etZbam3tJ8Wkdz1wpIdvcuR8eC/6NiJkpYuOwjKctiBzenlbAoMge', '44324233423', 0),
(6, 'Guloseima de Andrade', 'ananias@a.copm', '12312312312', '$2y$10$NxdNgO8q83QT/cAjw6CfDevOddrwwLzioalsbeTmVvaNryyjL9BK.', '$2y$10$ILKOGr2eUv8R7hFWaDCPPukaMEF7HCleXxua7vQ5sxg0CVmPwLRP2', '213.231.231', 0),
(7, 'Jubiscleia', '123123@1.com', '12312312312', '$2y$10$zXMH3P7hfL.bxgIU4WoXMeqxmnppwe2M/xL7wI9ItBrF3UGpmGAM2', '$2y$10$Hgm.qVBAGZ55Dipv8E80FORXIq8qShIrp1H55ZqBKIAhvaNq5GHV.', '228.046.038', 0),
(8, 'Muniscléia', 'nn@mm.com', '123123123', '$2y$10$2dW6tUxtKtP/J5pxbsIIOOhj.krxVyKVt2lgZOKEgFA8IVjra1.5K', '$2y$10$obXPiv.qYLe0dgCYR0jQc.TWCYQyJEPvw/Rn04GujHCMsYqt1brNO', '81888928399', 0),
(9, 'anineia', '12@323.cok', '1234456789', '$2y$10$KHI4CJwogPqkB6UiKY8T7u8DvGc/RhaO27ryzXoZ/W3GQuGJzS45i', '$2y$10$o7a9sXhO52PEX7GTewSzw.Dv4AkEZJzzf1L3NUkSQRxeZ9Zmyannq', '1122311132', 0),
(10, 'Alibaba', '2@11123.com', '123123', '$2y$10$sxrqzAI00mTh4I80U3czIOIiYAARPaFVYaebQRe98/wZXtTatr.tC', '$2y$10$6MINqSHFWi6UvwKOFpJw0.qUcgslyKq22W5Y3aMC44TzWoIeJoGEu', '1231233', 0),
(11, 'Alan Bóia Louca', 'a@a.com', '123123123', '$2y$10$BijcgyxfDykuC0XgEvf1iu8AOhtE3gsG4rLrhBaSW06IVIHvanyri', '$2y$10$es4y/srxIxLX4CzK42TbnOyAHI0nWcZvZL2aQiG.hT7fIH3HlVG7C', '8898', 0),
(12, 'unidos', '12@12.com', '12312312323', '$2y$10$3uEu6IRGcCgpwfDIjLP5huibze/BdVLQfFHse8eGEZ1aNqRjZwe.m', '$2y$10$Karc9ZKDHK9dt42mwgPcqumQGUahUt.N5koBkyDshlvjzE.HnQl.m', '1231231231', 0),
(13, 'ininin', '123@amks.com', '12312453436', '$2y$10$zxD4nHpDdkE7eDtwSUaOJuVB3o79WWtUHWRvQPt591SseubKZyPHi', '$2y$10$84ru0ABr007rYHkHLJltMODXpOqAw57b1FD4lIA2mFqgWghOY5F4.', '123123123', 0),
(14, 'asd', '1@1.com', '123146', '$2y$10$rv3wAWdrsTV0P1cAZWNw7OoD7C/PxNHC9R0iZChnZJVBQ7Phm6ixm', '$2y$10$SdadNSwpznqoOeAQ3RM9zOd37uDugzooq.0iHlFsbvhkHFnCm9dPq', 'asdasd', 0),
(15, 'asasa', 'aassa@asasa.com', '123123', '$2y$10$ymFZfTtp5q8hz/4sfYt28u/YuQ5PHhqRTyUqcDNKfzZxeWtK98V.W', '$2y$10$nPSZpKcOPNurU783KiWMJ.LVCfj735hH159on0ILB08h2XV5OgXXS', 'assassa', 0),
(17, 'Bruno Russo', 'brunor.producao@gmail.com', NULL, '$2y$10$LJkNY5x46NIhxUewgqmHzuw1KHs9Gc//EbQgHKJYb7ktSLfDy3sVK', '$2y$10$ZE5Hqofm29VcksAUHCe.cOurwrlqRvYSIKL1Owk8IB/4FdSwFdkDW', NULL, 0),
(18, 'Animal dee Teta', 'kndfkdf@ksjfjid.com', '123123123', '$2y$10$CC5YEhcblOI/9Ouv.VzF2.kVa/oQNNuLT88qYlMCsAt7aoo5OsuH.', '$2y$10$ucnLLeSgddQchmjEuGUx2O303D5dvVRkQ3GnZ2JjB1dU2CBRn6bVC', '12312312312', 0),
(19, 'masmka', 'bruno@cloudysky.com.br', '234edfgdgfa', '$2y$10$DroLCQcpVWEJ51XHjLs/3eAh0zgEZXYhbtq1RJkSsWyPTReegjuDi', '$2y$10$2YLaqg2B4HDEUE9dfYmSyukSIVh1iQgC5mTezdrF/ujM5JwZTxkNS', '12312354', 0),
(20, 'aaaaaaaaaaaab', 'wsdknf@jfis.coij', '1234556789', '$2y$10$gq5TN/mo.Ov3qiCzI3QQj.hVOYvVEroRGRaFrIj.B/6lZSm3A2fIW', '$2y$10$LlnO2CPbpRbD2gTxIRE3gO.zQaVDms0A0NvgKyYe0jo7hT89zIFfe', '12312312', 0),
(21, 'aaaaaaaaaaaaaaa', 'kmgmkf@fkf.com', '123678765', '$2y$10$dGgL4G6JMOVkZ/skrNfGCOx/UKz3ZipD8fQ6G9VZgVLMm/4xpojqy', '$2y$10$fOUq0MkrKmjEJvoEkh.dvOibIN7gXV.zm46aDBBcCVSwzI76dYhZ6', '1235686454', 0),
(22, 'Ana Luiza Gonçalves', 'ana@nanan.com', '12356774545', '$2y$10$CbHsYCZdN2TZM3J3OTDfP.FNcclopNsFgxJJFRZQYaY.llM8b2hRe', '$2y$10$6W.r5Ds9ud1L.q5eP0VPGODohgGQ73MrVN2giDgd2JgjKvB/fJr8q', '76368788278', 0),
(23, 'asdfasdf', 'weffsdt@fsdgh.dfgg', '3245658efd', '$2y$10$dA4cLCQd/sexIyVpFCLr6OhOr5A74Y0XsJcJ0otQILz8vd4ofHlRa', '$2y$10$TpeQsJ/L7YfgymRjF4E8Ae1xsWIrvsBNw/OvccZIDw4vNKHRRCa7G', 'sdaf2345346', 0),
(24, 'aaaaaaaaaaaaaaaaaaaa gora', 'agora@lkd.com', '12345678987', '$2y$10$2wL6ddN3D1I4R2k7OaECRueb5U.AQhB4KwiP5rIGIkOKcVdZFYOqm', '$2y$10$FpxqdfkNVSESKUvgGataZ.rk8u8EzAHlfeifDAf77AVM9KVuv62yK', '32488888888', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `item_venda`
--

DROP TABLE IF EXISTS `item_venda`;
CREATE TABLE IF NOT EXISTS `item_venda` (
  `id_venda` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `qtd` int(11) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id_venda`,`id_produto`),
  KEY `id_produto` (`id_produto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos`
--

DROP TABLE IF EXISTS `produtos`;
CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `fabricante` varchar(45) NOT NULL,
  `descricao` text NOT NULL,
  `preco` decimal(10,0) NOT NULL,
  `quantidade` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `fabricante`, `descricao`, `preco`, `quantidade`) VALUES
(1, 'Camomila', 'Jhonson & Jhonson\'s', 'qualquer coisa', '10', 10),
(2, 'Camomila', 'Jhonson & Jhonson\'s', 'qualquer coisa', '10', 10),
(3, 'Camomila', 'Jhonson & Jhonson\'s', 'qualquer coisa', '10', 10),
(4, 'Camomila', 'Jhonson & Jhonson\'s', 'qualquer coisa', '10', 10),
(5, 'Ana', 'banana', 'f', '123', 123),
(6, 'Fandamgos', '12345', 'huashusa', '333', 123),
(7, 'Jana', 'Ina', 'Lambda core', '123123', 2),
(8, 'Lambardina', 'cambuína', 'anali', '999', 99);

-- --------------------------------------------------------

--
-- Estrutura para tabela `vendas`
--

DROP TABLE IF EXISTS `vendas`;
CREATE TABLE IF NOT EXISTS `vendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vendedor` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `dt_venda` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`,`id_vendedor`,`id_cliente`),
  KEY `id_vendedor` (`id_vendedor`),
  KEY `id_cliente` (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `vendas`
--

INSERT INTO `vendas` (`id`, `id_vendedor`, `id_cliente`, `dt_venda`, `total`) VALUES
(1, 1, 2, '2017-11-27 14:39:07', '120.00'),
(2, 2, 10, '2017-11-27 14:44:16', '1202.00');

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `item_venda`
--
ALTER TABLE `item_venda`
  ADD CONSTRAINT `item_venda_ibfk_1` FOREIGN KEY (`id_venda`) REFERENCES `vendas` (`id`),
  ADD CONSTRAINT `item_venda_ibfk_2` FOREIGN KEY (`id_produto`) REFERENCES `produtos` (`id`);

--
-- Restrições para tabelas `vendas`
--
ALTER TABLE `vendas`
  ADD CONSTRAINT `vendas_ibfk_1` FOREIGN KEY (`id_vendedor`) REFERENCES `admin` (`id`),
  ADD CONSTRAINT `vendas_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
